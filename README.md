Yii2 Storage
================
Donar

<img src="https://cdn.neoacevedo.co/Litecoin-20201201-104326.png" alt="" width="320" />

**litecoin:LLv9oNeAJzhni4r6F2gWbcGCNVXf8BiRFe**

<img src="https://cdn.neoacevedo.co/Ethereum-20201201-104531.png" alt="" width="320" />

**ethereum:0x485d670cc27dd098DED63FDf94b7237d6Da891BE**

---
[![Latest Stable Version](https://poser.pugx.org/neoacevedo/yii2-storage/v/stable)](https://packagist.org/packages/neoacevedo/yii2-storage)
[![Total Downloads](https://poser.pugx.org/neoacevedo/yii2-storage/downloads)](https://packagist.org/packages/neoacevedo/yii2-storage)
[![Latest Unstable Version](https://poser.pugx.org/neoacevedo/yii2-storage/v/unstable)](https://packagist.org/packages/neoacevedo/yii2-storage)
[![License](https://poser.pugx.org/neoacevedo/yii2-storage/license)](https://packagist.org/packages/neoacevedo/yii2-storage)
[![Monthly Downloads](https://poser.pugx.org/neoacevedo/yii2-storage/d/monthly)](https://packagist.org/packages/neoacevedo/yii2-storage)
[![Daily Downloads](https://poser.pugx.org/neoacevedo/yii2-storage/d/daily)](https://packagist.org/packages/neoacevedo/yii2-storage)

Gestión de almacenamiento para Yii2.

\#storage \#module \#upload \#file \#extension \#aws \#azure \#google

Instalación
------------

La forma preferida de instalar esta extensión es a través de [composer](http://getcomposer.org/download/).

Luego ejecute

```
php composer.phar require --prefer-dist neoacevedo/yii2-storage
```

o agregue

```js
"neoacevedo/yii2-storage": "*"
```

a la sección require de su archivo `composer.json`.

Uso
-----

Una vez que la extensión está instalada, configure las credenciales para el servicio de almacenamiento en el archivo de configuración de su aplicación Yii  : 

```php
<?php

'components' => [
	/**
	 * Amazon S3
	 */ 
	'storageAWS' => [
	    'class' => 'neoacevedo\yii2\Storage',
	    'service' => 's3',
	    'config' => [
	        'key' => 'YOUR_IAM_ACCESS_KEY',
	        'secret' => 'YOUR_IAM__SECRET_ACCESS_KEY',
	        'bucket' => 'your-bucket',
	        'region' => 'your-region',
	        'extensions' => 'pdf, jpg, jpeg, gif, png, bmp'
	    ],
	    'prefix' => '', // ruta al directorio de imágenes. Ej: images/ (Opcional)
	]


	/**
	 * Azure Storage Blob
	 */
	'storageAzure' => [
	    'class' => 'neoacevedo\yii2\Storage',
	    'service' => 'azure',
	    'config' => [
	        'accountName' => 'ACCOUNT_NAME',
	        'accountKey' => 'ACCOUNT_KEY',
	        'container' => 'your-container',
	        'extensions' => 'pdf, jpg, jpeg, gif, png, bmp'
	    ],
	    'prefix' => '' // ruta al directorio de imágenes. Ej: images/ (Opcional)
	]

	/**
	 * Google Storage Cloud
	 */
	'storageGoogle' => [
	    'class' => 'neoacevedo\yii2\Storage',
	    'service' => 'gsc',
	    'config' => [
	        'projectId' => 'YOUR_PROJECT_ID',
	        'bucket' => 'your-bucket'
	        'keyFile' => '', // Contenido del archivo JSON generado en la consola de Google
	        'extensions' => 'pdf, jpg, jpeg, gif, png, bmp'
	    ],
	    'prefix' => '', // ruta al directorio de imágenes. Ej: images/ (Opcional)
	]

	/**
	 * Almacenamiento local
	 */ 
	'storageLocal' => [
	    'class' => 'neoacevedo\yii2\Storage',
	    'service' => 'local',
	    'config' => [
	        'baseUrl' => Yii::$app->request->hostInfo, // ej: http://example.com/
	        'directory' => '@webroot/web/uploads/', // reemplace @webroot por @frontend o @backend según sea el caso. La ruta debe terminar con una barra diagonal
	        'extensions' => 'pdf, jpg, jpeg, gif, png, bmp'
	    ],
	    'prefix' => '', // ruta al directorio de imágenes. Ej: images/ (Opcional) (Opcional)
]
```

Ahora puede llamarlo desde su aplicación :

```php
/**
 * Sube el archivo de imagen.
 * @param yii\web\UploadedFile $imageFile
 * @return boolean
 */
public function upload($imageFile)
{
    if (null !== $imageFile) {
        Yii::$app->storage->uploadedFile = $imageFile;
        return Yii::$app->storage->save();
    } else {
        return false;
    }
}

...
// obtener la URL generada
echo Yii::$app->storage->getUrl(Yii::$app->storage->prefix.$model->image_file); 
```


O simplemente en su código  :

```php
<?php 
use neoacevedo\yii2\Storage;

public function upload()
{
	$storage = new Storage([
		'service' => Storage::AWS_S3,
		'config' => [
			'key' => 'YOUR_IAM_ACCESS_KEY',
			'secret' => 'YOUR_IAM_SECRET_ACCESS_KEY',
			'bucket' => 'your-bucket',
			'region' => 'your-region',
			'extensions' => 'pdf, jpg, jpeg, gif, png, bmp'
		],
		'prefix' => '' // opcional
	]);
	
	$storage->uploadedFile = \yii\web\UploadedFile::getInstanceByName("file");
	return $storage->save();
}
```

Puede usar el modelo del componente en su formulario de las siguientes maneras en su controlador:

```php
...
// Constructor de clase
	$storage = new Storage([
		'service' => Storage::AWS_S3,
		'config' => [
			'key' => 'YOUR_IAM_ACCESS_KEY',
			'secret' => 'YOUR_IAM_SECRET_ACCESS_KEY',
			'bucket' => 'your-bucket',
			'region' => 'your-region',
			'extensions' => 'pdf, jpg, jpeg, gif, png, bmp'
		],
		'prefix' => '' // opcional
	]);
	return $this->render('create', [
                'model' => $model,
                'fileModel' => $storage->getModel()
    ]);	
...
// Como componente
	return $this->render('create', [
                'model' => $model,
                'fileModel' => Yii::$app->storage->getModel()
    ]);
...
// Usando el modelo de manera directa
	return $this->render('create', [
                'model' => $model,
                'fileModel' => new neoacevedo\yii2\storage\models\FileManager()
    ]);
```

Dentro de la vista:

```php
<?= $form->field($fileModel, 'uploadedFile')->fileInput() ?>
```
